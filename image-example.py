import matplotlib.pyplot as plt
from pprint import pprint
import numpy as np
from IPython.core.debugger import set_trace
from ppt.utils import attr
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
from ppt.utils import DogsCatsDataset
from torchvision import transforms
from torch.utils.data import DataLoader

train_ds = DogsCatsDataset("../data/raw", "sample/train")
# !tree -d ../data/raw/dogscats/
# train_ds

# the __len__ method
# len(train_ds)

# the __getitem__ method
# train_ds[0]

# train_ds[14][0]

for img, label_id in train_ds:
    print(label_id, train_ds.classes[label_id])
    display(img)

# Transforms
# Common image transformation that can be composed/chained.



IMG_SIZE = 224
_mean = [0.485, 0.456, 0.406]
_std = [0.229, 0.224, 0.225]


trans = transforms.Compose([
    transforms.RandomCrop(IMG_SIZE),
    # transforms.RandomHorizontalFlip(),
    # transforms.ColorJitter(.3, .3, .3),
    # transforms.ToTensor(),
    # transforms.Normalize(_mean, _std),
])

trans(train_ds[13][0])


# https://pytorch.org/docs/stable/torchvision/transforms.htm
# https://pytorch.org/tutorials/beginner/data_loading_tutorial.html
# https://github.com/mdbloice/Augmentor
# https://github.com/aleju/imgaug
# Shout-out:

# Hig performance image augmentation with pillow-simd: https://github.com/uploadcare/pillow-simd and http://python-pillow.org/pillow-perf/
# Improving Deep Learning Performance with AutoAugment (paper | pytorch implementation)
# Dataloader
# The DataLoader class offers batch loading of datasets with multi-processing and different sample strategies.



# DataLoader?

train_dl = DataLoader(train_ds, batch_size=2, shuffle=True, num_workers=4)

train_iter = iter(train_dl)
X, y = next(train_iter)

train_ds = DogsCatsDataset("../data/raw", "sample/train", transform=trans)
train_dl = DataLoader(train_ds, batch_size=2, shuffle=True, num_workers=4)

train_iter = iter(train_dl)
X, y = next(train_iter)
print("X:", X.shape)
print("y:", y.shape)